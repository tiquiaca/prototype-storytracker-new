﻿using BBT.Tapco.Prototype.Domain.Common.Events.Dispatcher;

namespace BBT.Tapco.Prototype.Domain.Common.Events
{
    public static class DomainEvents
    {
        public static IEventDispatcher Dispatcher { get; set; }

        public static void Raise<T>(T @event) where T : IDomainEvent
        {
            Dispatcher.Dispatch(@event);
        }
    }
}