﻿namespace BBT.Tapco.Prototype.Domain.Common.Events
{
    public interface IEventSubmission
    {
        void Submit<T>(T @event) where T : IDomainEvent;
    }
}