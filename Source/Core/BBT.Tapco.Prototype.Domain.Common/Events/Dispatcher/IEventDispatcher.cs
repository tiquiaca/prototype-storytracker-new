﻿namespace BBT.Tapco.Prototype.Domain.Common.Events.Dispatcher
{
    public interface IEventDispatcher
    {
        void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent;
    }
}