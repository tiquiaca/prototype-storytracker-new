﻿using BBT.Tapco.Prototype.Domain.Common.DependencyInversion;
using BBT.Tapco.Prototype.Domain.Common.Extensions;

namespace BBT.Tapco.Prototype.Domain.Common.Events.Dispatcher
{
    public class InjectableEventDispatcher : IEventDispatcher
    {
        private readonly IDependencyContainer _container;

        public InjectableEventDispatcher(IDependencyContainer container)
        {
            _container = container;
        }

        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            AssertionConcern.AssertArgumentNotNull(eventToDispatch, "Cannot dispatch a null event!");
            var handlers = _container.ResolveAll<IDomainEventHandler<TEvent>>();
            handlers?.ForEach(handler => handler.Handle(eventToDispatch));
        }
    }
}