﻿namespace BBT.Tapco.Prototype.Domain.Common.Modeling
{
    public interface IIdentity
    {
        string Id { get; }
    }
}
