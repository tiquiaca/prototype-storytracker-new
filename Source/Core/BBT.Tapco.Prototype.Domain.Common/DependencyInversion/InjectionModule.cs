﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BBT.Tapco.Prototype.Domain.Common.DependencyInversion
{
    public class InjectionModule
    {
        private readonly ConcurrentBag<NamedInstance> _instances = new ConcurrentBag<NamedInstance>();
        private readonly ConcurrentBag<NamedKeyValuePair> _maps = new ConcurrentBag<NamedKeyValuePair>();
        private readonly ConcurrentBag<NamedType> _types = new ConcurrentBag<NamedType>();

        public IReadOnlyList<NamedKeyValuePair> Maps => _maps.ToList();

        public IReadOnlyList<NamedInstance> Instances => _instances.ToList();

        public IReadOnlyList<NamedType> Types => _types.ToList();

        public InjectionModule Map<T, TR>() where TR : T
        {
            Map<T, TR>(null, LifetimeScope.Transient);
            return this;
        }
        
        public InjectionModule Map<T, TR>(LifetimeScope scope) where TR : T
        {
            Map<T, TR>(null, scope);
            return this;
        }
        
        public InjectionModule Map<T, TR>(string name) where TR : T
        {
            Map<T, TR>(name, LifetimeScope.Transient);
            return this;
        }
        
        public InjectionModule Map<T, TR>(string name, LifetimeScope scope) where TR : T
        {
            var pair = new NamedKeyValuePair(typeof (T), typeof (TR), name, scope);
            _maps.Add(pair);
            return this;
        }

        public InjectionModule Map(Type from, Type to)
        {
            Map(from, to, null, LifetimeScope.Transient);
            return this;
        }
        
        public InjectionModule Map(Type from, Type to, LifetimeScope scope)
        {
            Map(from, to, null, scope);
            return this;
        }

        public InjectionModule Map(Type from, Type to, string name)
        {
            Map(from, to, name, LifetimeScope.Transient);
            return this;
        }

        public InjectionModule Map(Type from, Type to, string name, LifetimeScope scope)
        {
            if (from == null)
            {
                const string message = @"Parameter from cannot be null in registration";
                throw new ArgumentNullException(nameof(from), message);
            }

            if (to == null)
            {
                const string message = @"Parameter to cannot be null in registration";
                throw new ArgumentNullException(nameof(to), message);
            }

            if (!from.GetTypeInfo().IsAssignableFrom(to))
            {
                string message = string.Format(@"The type {0} is not assignable from {1}", from.Name, to.Name);
                throw new ArgumentException(message);
            }

            var pair = new NamedKeyValuePair(from, to, name, scope);
            _maps.Add(pair);
            return this;
        }
        
        public InjectionModule AddType<T>() where T : class
        {
            AddType<T>(null, LifetimeScope.Transient);
            return this;
        }
        
        public InjectionModule AddType<T>(LifetimeScope scope) where T : class
        {
            AddType<T>(null, scope);
            return this;
        }
        
        public InjectionModule AddType<T>(string name) where T : class
        {
            AddType<T>(name, LifetimeScope.Transient);
            return this;
        }
        
        public InjectionModule AddType<T>(string name, LifetimeScope scope) where T : class
        {
            var type = new NamedType(typeof (T), name, scope);
            _types.Add(type);
            return this;
        }

        public InjectionModule AddInstance<T>(T instance) where T : class
        {
            AddInstance(instance, instance?.GetType(), null);
            return this;
        }
        
        public InjectionModule AddInstance<T>(T instance, string name) where T : class
        {
            AddInstance(instance, instance?.GetType(), name);
            return this;
        }

        public InjectionModule AddInstance<T>(T instance, Type type)
        {
            AddInstance(instance, type, null);
            return this;
        }
        
        public InjectionModule AddInstance<T>(T instance, Type type, string name)
        {
            if (instance == null)
            {
                const string message = @"Cannot assign a null instance to the container";
                throw new ArgumentNullException(nameof(instance), message);
            }

            if (type == null)
            {
                const string message = @"Cannot add an instance without specifying its resolution type";
                throw new ArgumentNullException(nameof(type), message);
            }

            var namedInstance = new NamedInstance(instance, type, name);
            _instances.Add(namedInstance);

            return this;
        }

        public class NamedInstance
        {
            public NamedInstance(object instance, string name = null) 
                : this(instance, instance.GetType(), name)
            {

            }

            public NamedInstance(object instance, Type type, string name = null)
                : this(new KeyValuePair<Type, object>(type, instance), name)
            {

            }

            public NamedInstance(KeyValuePair<Type, object> pair, string name = null)
            {
                KeyValuePair = pair;
                Name = name;
            }

            public string Name { get; set; }
            public KeyValuePair<Type, object> KeyValuePair { get; set; }
        }

        public class NamedKeyValuePair
        {
            public NamedKeyValuePair(Type from, Type to, string name = null,
                LifetimeScope scope = LifetimeScope.Transient)
                : this(new KeyValuePair<Type, Type>(from, to), name, scope)
            {
            }

            public NamedKeyValuePair(KeyValuePair<Type, Type> pair, string name = null,
                LifetimeScope scope = LifetimeScope.Transient)
            {
                KeyValuePair = pair;
                Name = name;
                Scope = scope;
            }

            public string Name { get; set; }
            public KeyValuePair<Type, Type> KeyValuePair { get; set; }
            public LifetimeScope Scope { get; set; }
        }

        public class NamedType
        {
            public NamedType(Type type, string name = null, LifetimeScope scope = LifetimeScope.Transient)
            {
                Type = type;
                Name = name;
                Scope = scope;
            }

            public Type Type { get; set; }
            public string Name { get; set; }
            public LifetimeScope Scope { get; set; }
        }
    }
}