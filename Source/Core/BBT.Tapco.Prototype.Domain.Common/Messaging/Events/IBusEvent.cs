﻿using System;

namespace BBT.Tapco.Prototype.Domain.Common.Messaging.Events
{
    public interface IBusEvent
    {
        Guid Id { get; set; }
    }
}