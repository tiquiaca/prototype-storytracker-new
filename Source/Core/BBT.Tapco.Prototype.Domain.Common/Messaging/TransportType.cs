﻿namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public enum TransportType
    {
        SqlServer = 0,
        Msmq,
        RabbitMq
    }
}