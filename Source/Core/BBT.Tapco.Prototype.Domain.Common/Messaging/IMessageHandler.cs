﻿namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public interface IMessageHandler<T>
    {
        void Handle(T message);
    }
}