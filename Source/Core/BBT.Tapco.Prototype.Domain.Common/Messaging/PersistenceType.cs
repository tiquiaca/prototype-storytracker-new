﻿namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public enum PersistenceType
    {
        InMemoryPersistence = 0,
        NHibernate,
        EntityFramework
    }
}