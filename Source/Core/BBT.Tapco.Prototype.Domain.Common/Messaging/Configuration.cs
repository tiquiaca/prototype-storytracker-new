﻿namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public class Configuration
    {
        public string EndpointName { get; set; }
        public SerializationType SerializationType { get; set; }
        public PersistenceType PersistenceType { get; set; }
        public TransportType TransportType { get; set; }
        public bool AutoSubscribe { get; set; }
    }
}
