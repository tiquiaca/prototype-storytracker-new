namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters
{
    public class NewCharacterCommand
    {
        public NewCharacterCommand()
        {
            
        }

        public NewCharacterCommand(string name, string side)
        {
            Name = name;
            Side = side;
        }

        public string Name { get; set; }

        public string Side { get; set; }
    }
}