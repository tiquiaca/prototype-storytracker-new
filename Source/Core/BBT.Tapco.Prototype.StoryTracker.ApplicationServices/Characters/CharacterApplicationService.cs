﻿using System;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters
{
    public class CharacterApplicationService : ICharacterApplicationService
    {
        private readonly ICharacterRepository _characterRepository;

        public CharacterApplicationService(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public void NewCharacterWith(NewCharacterCommand newCharacterCommand)
        {
            // todo needs consistency guarantees here jshelton
            CharacterId id = _characterRepository.GetNextCharacterId();
            Character character = new Character(id, newCharacterCommand.Name, newCharacterCommand.Side);
            _characterRepository.Add(character);
        }

        #region Alex Task on Services
        public IEnumerable<Character> GetAllCharacters()
        {
            return _characterRepository.Get();
        }

        public Character GetCharacterById(int id)
        {
            return _characterRepository.GetCharacterById(id);
        }

        public void AddCharacter(Character character)
        {
            _characterRepository.Add(character);
        }

        public void UpdateCharacter(Character character)
        {
            _characterRepository.Update(character);
        }

        public void DeleteCharacter(Character character)
        {
            _characterRepository.Delete(character);
        }
        #endregion
    }
}