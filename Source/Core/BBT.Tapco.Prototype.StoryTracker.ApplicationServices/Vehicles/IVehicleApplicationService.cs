using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles
{
    public interface IVehicleApplicationService
    {
        void NewVehicleWith(NewVehicleCommand newVehicleCommand);
        IEnumerable<Vehicle> GetAllVehicle();
        Vehicle GetVehicleById(int id);
        void AddVehicle(Vehicle vehicle);
        void UpdateVehicle(Vehicle vehicle);
        void DeleteVehicle(Vehicle vehicle);
    }
}