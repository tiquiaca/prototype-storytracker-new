﻿using System;
using System.Collections.Generic;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles
{
    public class VehicleApplicationService : IVehicleApplicationService
    {
        private readonly IVehicleRepository _vehicleRepository;

        public VehicleApplicationService(IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            _vehicleRepository.Add(vehicle);
        }

        public void DeleteVehicle(Vehicle vehicle)
        {
            _vehicleRepository.Delete(vehicle);
        }

        public IEnumerable<Vehicle> GetAllVehicle()
        {
            return _vehicleRepository.Get();
        }

        public Vehicle GetVehicleById(int id)
        {
            return _vehicleRepository.GetVehicleById(id);
        }

        public void NewVehicleWith(NewVehicleCommand newVehicleCommand)
        {
            VehicleId id = _vehicleRepository.GetNextVehicleId();
            Vehicle vehicle = new Vehicle(id, newVehicleCommand.Name, newVehicleCommand.Type);
            _vehicleRepository.Add(vehicle);
        }

        public void UpdateVehicle(Vehicle vehicle)
        {
            _vehicleRepository.Update(vehicle);
        }
    }
}
