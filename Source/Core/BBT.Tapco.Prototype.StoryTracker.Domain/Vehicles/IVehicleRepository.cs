﻿using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles
{
    public interface IVehicleRepository
    {
        void Add(Vehicle vehicle);

        void Update(Vehicle vehicle);

        void Delete(Vehicle vehicle);

        IEnumerable<Vehicle> Get();

        VehicleId GetNextVehicleId();
        Vehicle GetVehicleById(int id);
    }
}