﻿using BBT.Tapco.Prototype.Domain.Common.Modeling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Users
{/// <summary>
/// Licuanan, John James
/// 12/21/2016
/// Class for User to be used by User class.
/// </summary>
    public class UserId : Identity
    {
        public UserId(int identifier) : base(identifier.ToString())
        {

        }

        public int Identifier => int.Parse(Id);
    }
}
