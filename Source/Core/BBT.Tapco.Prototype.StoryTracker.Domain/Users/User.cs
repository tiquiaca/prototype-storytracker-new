﻿using BBT.Tapco.Prototype.Domain.Common;
using BBT.Tapco.Prototype.Domain.Common.Modeling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Users
{/// <summary>
/// Licuanan, John James
/// 12/21/2016
/// Class for User.
/// </summary>
    public class User : Entity, IEquatable<User>
    {
        private string _userName;
        private string _userPassword;
        private string _userRole;

        public User(UserId userid, string userName, string userPassword, string userRole)
        {
            AssertionConcern.AssertArgumentNotNull(userid, @"Cannot instantiate a user without an identity!");
            AssertionConcern.AssertArgumentNotNull(userName, @"Cannot instantiate a user without a username!");
            AssertionConcern.AssertArgumentNotNull(userPassword, @"Cannot instantiate a user without a password!");

            UserId = userid;
            _userName = userName;
            _userPassword = userPassword;
            _userRole = userRole;
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                AssertionConcern.AssertArgumentNotNull(value, "Cannot set the username  to null!");
                _userName = value;
            }
        }

        public string UserPassword
        {
            get { return _userPassword; }
            set
            {
                AssertionConcern.AssertArgumentNotNull(value, "Cannot set the  password to null!");
                _userPassword = value;
            }
        }
        public string UserRole
        {
            get { return _userRole; }
            set
            {
                AssertionConcern.AssertArgumentNotNull(value, "Cannot set the  user role to null!");
                _userRole = value;
            }
        }

        public UserId UserId { get; }
        public bool Equals(User other)
        {
            throw new NotImplementedException();
        }
    }
}
