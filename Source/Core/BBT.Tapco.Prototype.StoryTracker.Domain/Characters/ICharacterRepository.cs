﻿using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Characters
{
    public interface ICharacterRepository
    {
        void Add(Character character);

        void Update(Character character);

        void Delete(Character character);

        IEnumerable<Character> Get();

        CharacterId GetNextCharacterId();

        Character GetCharacterById(int id);
    }
}
