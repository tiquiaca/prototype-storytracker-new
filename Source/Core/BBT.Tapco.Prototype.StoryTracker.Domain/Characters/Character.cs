﻿using System;
using BBT.Tapco.Prototype.Domain.Common;
using BBT.Tapco.Prototype.Domain.Common.Modeling;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Characters
{
    public class Character : Entity, IEquatable<Character>
    {
        private string _name;
        private string _side;

        public Character()
        {
        }

        public Character(CharacterId characterId, string name, string side)
        {
            AssertionConcern.AssertArgumentNotNull(characterId, @"Cannot instantiate a character without an identity!");
            AssertionConcern.AssertArgumentNotNull(name, @"Cannot instantiate a character without a name!");
            AssertionConcern.AssertArgumentNotNull(side, @"Cannot instantiate a character without a side!");

            CharacterId = characterId;
            _name = name;
            _side = side;
        }

        public CharacterId CharacterId { get; set; }

        public string Name
        {
            get { return _name; }
            set
            {
                AssertionConcern.AssertArgumentNotNull(value, "Cannot set the character name to null!");
                _name = value;
            }
        }

        public string Side
        {
            get { return _side; }
            set
            {
                AssertionConcern.AssertArgumentNotNull(value, "Cannot set the character side to null!");
                _side = value;
            }
        }

        public bool Equals(Character other)
        {
            return CharacterId.Equals(other.CharacterId);
        }
    }
}
