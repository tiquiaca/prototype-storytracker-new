﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using BBT.Tapco.Prototype.StoryTracker.Domain;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using Microsoft.Extensions.Logging;
using AutoMapper;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Validation;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Controllers
{
    //todo need authorization jshelton
    [Route("api/[controller]")]

    public class CharactersController : Controller
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly ICharacterMapper _characterMapper;
        private readonly ICharacterApplicationService _characterApplicationService;
        private readonly INewCharacterCommandMapper _newCharacterCommandMapper;
        private readonly StoryTrackerEventSource _log = StoryTrackerEventSource.Log;
        private readonly ICharacterValidator _characterValidator;
        private ILogger<CharactersController> _logger;


  

        public CharactersController(ICharacterRepository characterRepository, ICharacterMapper characterMapper, 
            ICharacterValidator characterValidator,
            ICharacterApplicationService characterApplicationService,
            INewCharacterCommandMapper newCharacterCommandMapper,
            ILogger<CharactersController> _logger)
        {
            _characterValidator = new CharacterValidator();
            _characterRepository = characterRepository;
            _characterMapper = characterMapper;
            _characterApplicationService = characterApplicationService;
            _newCharacterCommandMapper = newCharacterCommandMapper;
        }

        // GET api/values
        [HttpGet]
        public IActionResult GetCharacters()
        {
            try
            {
                 _log.StartTheRequestWasReceivedToReturnEveryCharacter();
                var characters =  _characterApplicationService.GetAllCharacters();

                ///temporary only
                var newCharacters = new List<Common.Resources.Character>();
                foreach (var i in characters)
                {
                    newCharacters.Add(new Common.Resources.Character
                    {
                        Id = int.Parse(i.CharacterId.Id),
                        Name = i.Name,
                        Side = i.Side
                    });
                }
                ///temporary only

                return Ok(newCharacters);
            }
            catch (Exception e)
            {
                _log.ErrorWhileExecutingRequestToReturnEveryCharacter(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while retrieving the characters");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToReturnEveryCharacter();
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var character = _characterApplicationService.GetCharacterById(id);//->



                var newCharacter = new Common.Resources.Character {
                    Id = int.Parse(character.CharacterId.Id),
                    Name=character.Name,
                    Side=character.Side
                };


                return Ok(newCharacter);
            }
            catch (Exception e)
            {
                _log.ErrorWhileExecutingRequestToReturnEveryCharacter(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while retrieving the characters");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToReturnEveryCharacter();
            }
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Common.Resources.Character character)
        {
            try
            {
                _log.StartTheRequestWasReceivedToAddCharacter();

                //IEnumerable<string> validation;
                //if (!_characterValidator.Validate(character, out validation))
                //    return BadRequest(validation);
                var result = _characterValidator.Validate(character);
                if (!result.IsValid) return BadRequest(result.Errors);

                //NewCharacterCommand newCharacterCommand = _newCharacterCommandMapper.Get(character);
                //_characterApplicationService.NewCharacterWith(newCharacterCommand);

                _characterApplicationService.AddCharacter(_characterMapper.Get(character));

                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToAddVehicle(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while saving the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToAddCharacter();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Common.Resources.Character character)
        {
            try
            {
                _log.StartTheRequestWasReceivedToUpdateCharacter();
                var result = _characterValidator.Validate(character);
                if (!result.IsValid) return BadRequest(result.Errors);

                _characterApplicationService.UpdateCharacter(_characterMapper.Get(character));

                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToUpdateCharacter(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while saving the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToUpdateCharacter();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromBody]Common.Resources.Character character)
        {
            try
            {
                _log.StartTheRequestWasReceivedToDeleteCharacter();
                //IEnumerable<string> validation;
                //if (!_characterValidator.Validate(character, out validation))
                //    return BadRequest(validation);

                var toDelete = _characterApplicationService.GetCharacterById(id);

                _characterApplicationService.DeleteCharacter(toDelete);

                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToDeleteCharacter(e.Message,e.StackTrace);
                return StatusCode(500, "An error occurred while deleting the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToDeleteCharacter();
            }
        }
    }
}
