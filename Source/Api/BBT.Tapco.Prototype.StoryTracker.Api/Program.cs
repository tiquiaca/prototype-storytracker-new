﻿using System.IO;
using System;
using Microsoft.AspNetCore.Hosting;

namespace BBT.Tapco.Prototype.StoryTracker.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
