﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Validation;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories.Mocks;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles;
using Autofac;
using System;
using Autofac.Extensions.DependencyInjection;
using NLog.Targets;
using NLog;
using NLog.Extensions.Logging;
using System.IO;
using System.Linq;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess;

namespace BBT.Tapco.Prototype.StoryTracker.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            //TQ Add CORS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    x => x.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
            // Adding dependencies using built in DI
            /*
            services.AddScoped<ICharacterRepository, MockCharacterRepository>();
            services.AddScoped<ICharacterMapper, CharacterMapper>();
            services.AddScoped<ICharacterApplicationService, CharacterApplicationService>();
            services.AddScoped<INewCharacterCommandMapper, NewCharacterCommandMapper>();
            services.AddScoped<ICharacterValidator, CharacterValidator>();

            services.AddScoped<IVehicleRepository, MockVehicleRepository>();
            services.AddScoped<IVehicleMapper, VehicleMapper>();
            services.AddScoped<IVehicleApplicationService, VehicleApplicationService>();
            services.AddScoped<INewVehicleComamndMapper, NewVehicleComamndMapper>();
            services.AddScoped<IVehicleValidator, VehicleValidator>();
            */

            #region Alex's Task on Autofac
            // To do: Figure out how to do registration of all dependencies using Assembly
            var builder = new ContainerBuilder();

            builder.RegisterType<DbFactory>().As<IDbFactory>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            builder.RegisterType<CharacterValidator>().As<ICharacterValidator> ();
            builder.RegisterType<VehicleValidator>().As<IVehicleValidator>();

            builder.RegisterType<CharacterPersistenceRepository>().As<ICharacterPersistenceRepository>();
            
            builder.RegisterType<VehiclePersistenceRepository>().As<IVehiclePersistenceRepository>();
            builder.RegisterType<CharacterRepository>().As<ICharacterRepository>();
            builder.RegisterType<CharacterMapper>().As<ICharacterMapper>();
            builder.RegisterType<CharacterApplicationService>().As<ICharacterApplicationService>();
            builder.RegisterType<NewCharacterCommandMapper>().As<INewCharacterCommandMapper>();
            builder.RegisterType<CharacterValidator>().As<ICharacterValidator>();
            builder.RegisterType<VehicleRepository>().As<IVehicleRepository>();
            builder.RegisterType<VehicleMapper>().As<IVehicleMapper>();
            builder.RegisterType<VehicleApplicationService>().As<IVehicleApplicationService>();
            builder.RegisterType<NewVehicleComamndMapper>().As<INewVehicleComamndMapper>();
            builder.RegisterType<VehicleValidator>().As<IVehicleValidator>();


            builder.Populate(services);

            var container = builder.Build();

            return container.Resolve<IServiceProvider>();
            #endregion
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            //add NLog
            loggerFactory.AddNLog();
            app.UseCors("CorsPolicy");

            var configDir = "C:\\git\\Logs";

            if (configDir != string.Empty)
            {
                foreach (DatabaseTarget target in LogManager.Configuration.AllTargets.Where(t => t is DatabaseTarget))
                {
                    target.ConnectionString = Configuration.GetConnectionString("NLogDb");
                }

                var logEventInfo = NLog.LogEventInfo.CreateNullEvent();
                foreach (FileTarget target in LogManager.Configuration.AllTargets.Where(t => t is FileTarget))
                {
                    var filename = target.FileName.Render(logEventInfo).Replace("'", "");
                    target.FileName = Path.Combine(configDir, filename);
                }

                LogManager.ReconfigExistingLoggers();
            }

            if (env.IsDevelopment())
            {
                // Need to use status code pages
                // Need to use developer friendly execption pages
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
