﻿using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers
{
    public interface INewVehicleComamndMapper
    {
        NewVehicleCommand Get(Resources.Vehicle character);
    }
}