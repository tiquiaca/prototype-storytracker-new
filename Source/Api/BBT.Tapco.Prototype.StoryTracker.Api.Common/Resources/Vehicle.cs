﻿namespace BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources
{
    public class Vehicle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
