﻿namespace BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources
{
    public class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Side { get; set; }
    }
}
