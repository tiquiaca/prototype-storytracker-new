﻿using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources;
using BBT.Tapco.Prototype.StoryTracker.Domain.Users;
using AutoMapper;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.ResourceMappers
{
    public class UserMappers : IUserMapper
    {
        //Gio Medina User Mapper

        #region getUser
        public Domain.Users.User Get(Api.Common.Resources.User user)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Api.Common.Resources.User, Domain.Users.User>());

            IMapper mapper = config.CreateMapper();

            var getUser = mapper.Map<Domain.Users.User>(user);

            return getUser;


        }

        public Api.Common.Resources.User Get(Domain.Users.User user)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Domain.Users.User, Api.Common.Resources.User>());

            IMapper mapper = config.CreateMapper();

            var getUser = mapper.Map<Api.Common.Resources.User>(user);

            return getUser;
        }

        #endregion
    }
}
