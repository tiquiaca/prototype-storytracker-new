﻿using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using AutoMapper;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources;
using System;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.ResourceMappers
{
    public class VehicleMapper : IVehicleMapper
    {

        //Gio Medina AutoMapper Implementation Domain to Resources, Resources to domain

        #region Mapper for getVehicle

        public Api.Common.Resources.Vehicle Get(Domain.Vehicles.Vehicle vehicle)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Domain.Vehicles.Vehicle, Api.Common.Resources.Vehicle>());

            IMapper mapper = config.CreateMapper();

            var getVehicle = mapper.Map<Api.Common.Resources.Vehicle>(vehicle);

            return getVehicle;
        }

     
        public Domain.Vehicles.Vehicle Get(Api.Common.Resources.Vehicle vehicle)
        {

            var config = new MapperConfiguration(cfg => cfg.CreateMap<Api.Common.Resources.Vehicle, Domain.Vehicles.Vehicle>());

            IMapper mapper = config.CreateMapper();

            var getVehicle = mapper.Map<Domain.Vehicles.Vehicle>(vehicle);

            return getVehicle;
        }
        #endregion

        #region Mapper for getAllVehicle
        public List<Domain.Vehicles.Vehicle> GetAll(List<Api.Common.Resources.Vehicle> vehicles)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap< List<Api.Common.Resources.Vehicle>, List<Domain.Vehicles.Vehicle>>());

            IMapper mapper = config.CreateMapper();

            var getAllVehicle = mapper.Map<List<Domain.Vehicles.Vehicle>>(vehicles);

            return getAllVehicle;
        }

        public List<Api.Common.Resources.Vehicle> GetAll(List<Domain.Vehicles.Vehicle> vehicles)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<List<Domain.Vehicles.Vehicle>, List<Api.Common.Resources.Vehicle>>());

            IMapper mapper = config.CreateMapper();

            var getAllVehicle = mapper.Map<List<Api.Common.Resources.Vehicle>>(vehicles);

            return getAllVehicle;
        }
        #endregion
    }
}