﻿using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using AutoMapper;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources;
using System;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.ResourceMappers
{
    public class CharacterMapper : ICharacterMapper
    {

        // Gio Medina Auto Mapper Implementation Domain to Resources, Resources to domain
        #region Mapping of getCharacter
        public Api.Common.Resources.Character Get(Domain.Characters.Character character)
        {

            var config = new MapperConfiguration(cfg => cfg.CreateMap<Domain.Characters.Character, Api.Common.Resources.Character>());

            IMapper mapper = config.CreateMapper();

            var getCharacter = mapper.Map<Api.Common.Resources.Character>(character);

            return getCharacter;
        }

        public Domain.Characters.Character Get(Api.Common.Resources.Character character)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Api.Common.Resources.Character, Domain.Characters.Character>());

            IMapper mapper = config.CreateMapper();

            var getCharacter = mapper.Map<Domain.Characters.Character>(character);

            return getCharacter;
        }
        #endregion
        #region Mapping of getAllCharacter
        public List<Domain.Characters.Character> GetAll(List<Api.Common.Resources.Character> characters)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<List<Api.Common.Resources.Character>, List<Domain.Characters.Character>>());

            IMapper mapper = config.CreateMapper();

            var getAllCharacter = mapper.Map<List<Domain.Characters.Character>>(characters);

            return getAllCharacter;
        }

        public List<Api.Common.Resources.Character> GetAll(List<Domain.Characters.Character> characters)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<List<Domain.Characters.Character>, List<Api.Common.Resources.Character>>());

            IMapper mapper = config.CreateMapper();

            var getAllCharacter = mapper.Map<List<Api.Common.Resources.Character>>(characters);

            return getAllCharacter;
        }
        #endregion

    }
}