﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Mappers;
using Vehicle = BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel.Vehicle;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories
{
    public interface IVehiclePersistenceRepository : IRepository<Vehicle>
    {

    }

    public class VehiclePersistenceRepository : RepositoryBase<Vehicle>, IVehiclePersistenceRepository
    {
        public VehiclePersistenceRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }


    public class VehicleRepository : IVehicleRepository
    {
        private readonly IVehiclePersistenceRepository _iVehiclePersistenceRepository;


        public VehicleRepository(IVehiclePersistenceRepository iVehiclePersistenceRepository)
        {
            _iVehiclePersistenceRepository = iVehiclePersistenceRepository;
        }

        public void Add(Domain.Vehicles.Vehicle vehicle)
        {
            var mapper = new VehicleMapper();
            var parsistenceVehicle = mapper.Get(vehicle);
            _iVehiclePersistenceRepository.Add(parsistenceVehicle);

        }

        public void Delete(Domain.Vehicles.Vehicle vehicle)
        {
            var mapper = new VehicleMapper();
            var parsistenceVehicle = mapper.Get(vehicle);
            _iVehiclePersistenceRepository.Delete(parsistenceVehicle);
        }

        public IEnumerable<Domain.Vehicles.Vehicle> Get()
        {
            var mapper = new VehicleMapper();
            var parsistenceVehicles = _iVehiclePersistenceRepository.GetAll();
            return mapper.GetList(parsistenceVehicles);

        }

        public Domain.Vehicles.Vehicle GetVehicleById(int id)
        {
            var mapper = new VehicleMapper();
            var vehicle = _iVehiclePersistenceRepository.GetById(id);
            return mapper.Get(vehicle);

        }

        public void Update(Domain.Vehicles.Vehicle vehicle)
        {
            throw new NotImplementedException();
        }

        public VehicleId GetNextVehicleId()
        {
            throw new NotImplementedException();
        }
    }
}