﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Mappers;
using User = BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel.User;
using BBT.Tapco.Prototype.StoryTracker.Domain.Users;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories
{
    public interface IUserPersistenceRepository : IRepository<User>
    {

    }

    public class UserPersistenceRepository : RepositoryBase<User>, IUserPersistenceRepository
    {
        public UserPersistenceRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public class UserRepository : IUserRepository
    {
        private readonly IUserPersistenceRepository _iUserPersistenceRepository;

        public void Add(Domain.Users.User user)
        {
            var mapper = new UserMapper();
            var parsistenceUser = mapper.Get(user);
            _iUserPersistenceRepository.Add(parsistenceUser);

        }

        public void Delete(Domain.Users.User user)
        {
            var mapper = new UserMapper();
            var parsistenceUser = mapper.Get(user);
            _iUserPersistenceRepository.Delete(parsistenceUser);
        }



        public Domain.Users.User GetUserById(int id)
        {
            var mapper = new UserMapper();
            var user = _iUserPersistenceRepository.GetById(id);
            return mapper.Get(user);

        }

        public void Update(Domain.Users.User user)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<Domain.Users.User> Get()
        {
            throw new NotImplementedException();
        }

        public UserId GetNextUserId()
        {
            throw new NotImplementedException();
        }
    }
}