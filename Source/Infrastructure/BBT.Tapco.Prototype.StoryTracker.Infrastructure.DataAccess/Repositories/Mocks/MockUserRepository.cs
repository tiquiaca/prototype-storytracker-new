﻿using BBT.Tapco.Prototype.StoryTracker.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories.Mocks
{/// <summary>
/// Licuanan, John James
/// 12/21/2016
/// Repository class for User
/// </summary>
    public class MockUserRepository : IUserRepository
    {
        private readonly IList<User> _user = new List<User>();

        public void Add(User user)
        {
            _user.Add(user);
        }


        public void Update(User user)
        {
            User foundUser = _user.FirstOrDefault(c => c.Equals(user));
            if (foundUser == null) return;
            foundUser.UserName = user.UserName;
            foundUser.UserPassword = user.UserPassword;
            foundUser.UserRole = user.UserRole;
        }

        public void Delete(User user)
        {
            User foundUser = _user.FirstOrDefault(c => c.Equals(user));
            if (foundUser == null) return;
            _user.Remove(foundUser);
        }

        public IEnumerable<User> Get()
        {
            return _user;
        }

        public UserId GetNextUserId()
        {
            int identifier = _user.Max(c => c.UserId.Identifier) + 1;
            return new UserId(identifier);
        }
    }

}
