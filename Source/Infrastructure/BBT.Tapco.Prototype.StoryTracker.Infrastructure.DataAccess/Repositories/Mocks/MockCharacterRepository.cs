﻿using System;
using System.Collections.Generic;
using System.Linq;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories.Mocks
{
    public class MockCharacterRepository : ICharacterRepository
    {
        private readonly IList<Character> _characters = new List<Character>();

        public void Add(Character character)
        {
            _characters.Add(character);
        }

        public void Update(Character character)
        {
            Character foundCharacter = _characters.FirstOrDefault(c => c.Equals(character));
            if (foundCharacter == null) return;
            foundCharacter.Name = character.Name;
            foundCharacter.Side = character.Side;
        }

        public void Delete(Character character)
        {
            Character foundCharacter = _characters.FirstOrDefault(c => c.Equals(character));
            if (foundCharacter == null) return;
            _characters.Remove(foundCharacter);
        }

        public IEnumerable<Character> Get()
        {
            return _characters;
        }

        public CharacterId GetNextCharacterId()
        {
            int identifier = _characters.Max(c => c.CharacterId.Identifier) + 1;
            return new CharacterId(identifier);
        }

        public Character GetCharacterById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
