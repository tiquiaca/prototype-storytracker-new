﻿using System;
using System.Collections.Generic;
using System.Linq;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories.Mocks
{
    public class MockVehicleRepository : IVehicleRepository
    {
        private readonly IList<Vehicle> _vehicles = new List<Vehicle>();

        public void Add(Vehicle character)
        {
            _vehicles.Add(character);
        }

        public void Update(Vehicle character)
        {
            Vehicle foundCharacter = _vehicles.FirstOrDefault(c => c.Equals(character));
            if (foundCharacter == null) return;
            foundCharacter.Name = character.Name;
            foundCharacter.Type = character.Type;
        }

        public void Delete(Vehicle character)
        {
            Vehicle foundCharacter = _vehicles.FirstOrDefault(c => c.Equals(character));
            if (foundCharacter == null) return;
            _vehicles.Remove(foundCharacter);
        }

        public IEnumerable<Vehicle> Get()
        {
            return _vehicles;
        }

        public VehicleId GetNextVehicleId()
        {
            int identifier = _vehicles.Max(c => c.VehicleId.Identifier) + 1;
            return new VehicleId(identifier);
        }

        public Vehicle GetVehicleById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
