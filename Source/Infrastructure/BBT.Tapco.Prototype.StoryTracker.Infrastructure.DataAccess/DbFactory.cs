﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess
{
    public class DbFactory : Disposable, IDbFactory
    {
        StoryTrackerDbContext _dbContext;

        public StoryTrackerDbContext Init()
        {
            return _dbContext ?? (_dbContext = new StoryTrackerDbContext());
        }

        protected override void DisposeCore()
        {
            _dbContext?.Dispose();
        }
    }
}
