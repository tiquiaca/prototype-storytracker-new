﻿using Domain = BBT.Tapco.Prototype.StoryTracker.Domain;
using Persistence = BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Mappers
{
    public class VehicleMapper
    {
        public Domain.Vehicles.Vehicle Get(Persistence.Vehicle vehicle)
        {
            return new Domain.Vehicles.Vehicle()
            {
                VehicleId = new Domain.Vehicles.VehicleId(vehicle.Id),
                Name =vehicle.Name,
                Type = vehicle.Type
            };
        }

        public Persistence.Vehicle Get(Domain.Vehicles.Vehicle vehicle)
        {
            return new Persistence.Vehicle()
            {
                Id = vehicle.VehicleId.Identifier,
                Name = vehicle.Name,
                Type = vehicle.Type
            };
        }

        public IEnumerable<Domain.Vehicles.Vehicle> GetList(IEnumerable<Persistence.Vehicle> vehicles)
        {
            var result = new List<Domain.Vehicles.Vehicle>();
            foreach (var item in vehicles)
            {
                result.Add(Get(item));
            }
            return result;
        }

        public List<Persistence.Vehicle> GetList(List<Domain.Vehicles.Vehicle> vehicles)
        {
            var result = new List<Persistence.Vehicle>();
            foreach (var item in vehicles)
            {
                result.Add(Get(item));
            }
            return result;

        }
    }
}
