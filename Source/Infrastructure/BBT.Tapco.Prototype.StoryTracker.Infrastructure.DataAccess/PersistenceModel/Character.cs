﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel
{
    public class Character
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Side { get; set; }
    }
}
