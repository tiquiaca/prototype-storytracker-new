﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel
{
    public class Vehicle
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
