import { PipeTransform } from '@angular/core';
export declare class InitCapsPipe implements PipeTransform {
    transform(value: string): string;
}
