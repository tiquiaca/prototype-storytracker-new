export declare class FilterTextService {
    constructor();
    filter(data: string, props: Array<string>, originalList: Array<any>): any[];
}
