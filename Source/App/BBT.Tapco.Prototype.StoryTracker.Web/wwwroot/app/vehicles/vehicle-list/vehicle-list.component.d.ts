import { OnDestroy, OnInit } from '@angular/core';
import { FilterTextComponent } from '../../shared/filter-text/filter-text.component';
import { FilterTextService } from '../../shared/filter-text/filter-text.service';
import { Vehicle } from '../shared/vehicle.model';
import { VehicleService } from '../shared/vehicle.service';
export declare class VehicleListComponent implements OnDestroy, OnInit {
    private filterService;
    private vehicleService;
    private dbResetSubscription;
    vehicles: Vehicle[];
    filteredVehicles: Vehicle[];
    filterComponent: FilterTextComponent;
    constructor(filterService: FilterTextService, vehicleService: VehicleService);
    filterChanged(searchText: string): void;
    getVehicles(): void;
    ngOnDestroy(): void;
    ngOnInit(): void;
    trackByVehicles(index: number, vehicle: Vehicle): number;
}
