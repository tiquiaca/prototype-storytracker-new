import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Vehicle } from './vehicle.model';
import { VehicleService } from './vehicle.service';
export declare class VehicleResolver implements Resolve<Vehicle> {
    private vehicleService;
    private router;
    constructor(vehicleService: VehicleService, router: Router);
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any;
}
