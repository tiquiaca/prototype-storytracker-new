import { OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Character, CharacterService } from '../../models';
import { CanComponentDeactivate, EntityService, ModalService, ToastService } from '../../core';
export declare class CharacterComponent implements OnDestroy, OnInit, CanComponentDeactivate {
    private entityService;
    private modalService;
    private route;
    private router;
    private characterService;
    private toastService;
    character: Character;
    editCharacter: Character;
    private dbResetSubscription;
    private id;
    constructor(entityService: EntityService, modalService: ModalService, route: ActivatedRoute, router: Router, characterService: CharacterService, toastService: ToastService);
    cancel(showToast?: boolean): void;
    canDeactivate(): true | Promise<boolean>;
    delete(): void;
    isAddMode(): boolean;
    ngOnDestroy(): void;
    ngOnInit(): void;
    save(): void;
    private getCharacter();
    private gotoCharacters();
    private handleServiceError(op, err);
    private isDirty();
    private setEditCharacter(character);
}
