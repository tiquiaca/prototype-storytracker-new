import { Http } from '@angular/http';
import { ToastService } from './toast/toast.service';
export interface ResetMessage {
    message: string;
}
export declare class MessageService {
    private http;
    private toastService;
    private subject;
    state: any;
    constructor(http: Http, toastService: ToastService);
    resetDb(): void;
}
