export declare let CONFIG: {
    baseUrls: {
        config: string;
        resetDb: string;
        characters: string;
        vehicles: string;
    };
};
