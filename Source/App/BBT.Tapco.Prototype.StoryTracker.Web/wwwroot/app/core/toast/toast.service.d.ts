export interface ToastMessage {
    message: string;
}
export declare class ToastService {
    private toastSubject;
    toastState: any;
    constructor(prior: ToastService);
    activate(message?: string): void;
}
