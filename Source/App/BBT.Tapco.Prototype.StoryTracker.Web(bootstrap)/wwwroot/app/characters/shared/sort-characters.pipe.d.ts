import { PipeTransform } from '@angular/core';
import { Character } from '../../models';
export declare class SortCharactersPipe implements PipeTransform {
    transform(value: Character[], args?: any[]): Character[];
}
