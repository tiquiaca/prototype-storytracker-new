import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Character } from './character.model';
import { CONFIG, ExceptionService, MessageService, SpinnerService } from '../core';

let charactersUrl = CONFIG.baseUrls.characters;

@Injectable()
export class CharacterService {
  onDbReset = this.messageService.state;

  constructor(private http: Http,
    private exceptionService: ExceptionService,
    private messageService: MessageService,
    private spinnerService: SpinnerService) {
    this.messageService.state.subscribe(state => this.getCharacters());
  }

  //caddCharacter(character: Character) {
  //  let body = JSON.stringify(character);
  //  this.spinnerService.show();
  //  return <Observable<Character>>this.http
  //    .post(`${charactersUrl}`, body)
  //    .map(res => res.json().data)
  //    .catch(this.exceptionService.catchBadResponse)
  //    .finally(() => this.spinnerService.hide());
  //}



  // Add a new character
  addCharacter(character: Character): Observable<Character> {
      let body = JSON.stringify(character); // Stringify payload
      let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
      let options = new RequestOptions({ headers: headers }); // Create a request option
      this.spinnerService.show();

      return this.http.post(`${charactersUrl}`, body, options) // ...using post request
          .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
          .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

  }





  deleteCharacter(character: Character) {
    this.spinnerService.show();
    return <Observable<Character>>this.http
      .delete(`${charactersUrl}/${character.id}`)
      .map(res => this.extractData<Character>(res))
      .catch(this.exceptionService.catchBadResponse)
      .finally(() => this.spinnerService.hide());
  }

  // Fetch all existing Characters
  getCharacters(): Observable<Character[]> {

      return this.http.get(charactersUrl)          // ...and calling .json() on the response to return data
          .map((res: Response) => res.json())          //...errors if any
          .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  //getCharacter(id: number) {
  //  this.spinnerService.show();
  //  return <Observable<Character>>this.http
  //    .get(`${charactersUrl}/${id}`)
  //    .map(res => this.extractData<Character>(res))
  //    .catch(this.exceptionService.catchBadResponse)
  //    .finally(() => this.spinnerService.hide());
  //}

  // Get a Character
  getCharacter(id: number): Observable<Character> {

      this.spinnerService.show();

      return this.http.get(`${charactersUrl}/${id}`) // ...using put request
          .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
          .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }




  updateCharacter(character: Character) {
    let body = JSON.stringify(character);
    this.spinnerService.show();

    return <Observable<Character>>this.http
      .put(`${charactersUrl}/${character.id}`, body)
      .map(res => this.extractData<Character>(res))
      .catch(this.exceptionService.catchBadResponse)
      .finally(() => this.spinnerService.hide());
  }

  private extractData<T>(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }
    let body = res.json ? res.json() : null;
    return <T>(body && body.data || {});
  }
}
