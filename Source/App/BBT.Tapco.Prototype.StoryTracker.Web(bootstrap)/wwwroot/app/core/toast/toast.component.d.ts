import { OnDestroy, OnInit } from '@angular/core';
import { ToastService } from './toast.service';
export declare class ToastComponent implements OnDestroy, OnInit {
    private toastService;
    private defaults;
    private toastElement;
    private toastSubscription;
    title: string;
    message: string;
    constructor(toastService: ToastService);
    activate(message?: string, title?: string): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    private show();
    private hide();
}
